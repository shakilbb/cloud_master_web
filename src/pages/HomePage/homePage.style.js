import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
`;

export const HeaderContainer = styled.div`
  display: flex;
  width: 100%;
  height: 8rem;
  align-items: center;
  box-shadow: 0 0 1.2rem #AAA;
  position: fixed;
  z-index: 2;
  background-color: white;
  padding-left: 4rem;
`;

export const ImageContainer = styled.div`
  width: 8rem;
  cursor: pointer;
`;

export const Image = styled.img`
  width: 100%;
`;

export const Header = styled.div`
  font-size: 2.4rem;
  text-align: center;
  font-family: 'LatoBold', arial, sans-serif;
`;

export const BodyContainer = styled.div`
  margin: 10rem 1.2rem 0;
  height: calc(100vh - 10rem);
`;
