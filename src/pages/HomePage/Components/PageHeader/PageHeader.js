import { useState } from 'react';
import Link from 'next/link';

import {
  HeaderContainer,
  Title,
  ImageContainer,
  Image,
  LeftHead,
  RightHead,
  LogoContainer,
  Logo,
  InputContainer,
  CloseContainer,
  Input,
} from './pageHeader.style';

const Header = () => {
  const [showSearch, setSearchStatus] = useState(false);
  const [searchText, setSearchText] = useState(false);
  return (
    <HeaderContainer>
      <LeftHead>
        <ImageContainer>
          <Link href="/" as="/home">
            <Image src="/logo.png" alt="logo" />
          </Link>
        </ImageContainer>
        <Title>The Cloud Master</Title>
      </LeftHead>
      <RightHead>
        <LogoContainer onClick={() => setSearchStatus(!showSearch)} showSearch={showSearch}>
          <Logo src="/search.png" alt="search" />
        </LogoContainer>
        <InputContainer showSearch={showSearch}>
          <Input value={searchText} onChange={ev => setSearchText(ev.target.value)} />
          <CloseContainer onClick={() => setSearchStatus(!showSearch)}>
            <Logo src="/close.png" alt="close" />
          </CloseContainer>
        </InputContainer>
      </RightHead>
    </HeaderContainer>
  );
};

export default Header;
