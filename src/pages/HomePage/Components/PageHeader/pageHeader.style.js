import styled, { keyframes, css } from 'styled-components';

import { Input as AntdInput } from 'antd';

const searchAnimation = keyframes`
  from {
    width: 0rem;
  }

  to {
    width: 8rem;
  }
`;

export const HeaderContainer = styled.div`
  display: flex;
  width: 100%;
  height: 8rem;
  align-items: center;
  box-shadow: 0 0 1.2rem #AAA;
  position: fixed;
  z-index: 2;
  background-color: white;
  justify-content: space-between;
`;

export const ImageContainer = styled.div`
  width: 8rem;
  cursor: pointer;
`;

export const Image = styled.img`
  width: 100%;
`;

export const Title = styled.div`
  font-size: 2.4rem;
  text-align: center;
  font-family: 'LatoBold', arial, sans-serif;
`;

export const LeftHead = styled.div`
  display: flex;
  align-items: center;
`;

export const RightHead = styled(LeftHead)`
`;

export const LogoContainer = styled(ImageContainer)`
  width: 2.4rem;
  display: ${props => props.showSearch ? 'none' : 'flex'};
`;

export const Logo = styled(Image)`
`;

const animation = css`
  animation: ${searchAnimation} 1s forwards;
`;

export const InputContainer = styled.div`
  width: 0rem;
  height: 4rem;
  ${props => props.showSearch && animation}
`;

export const Input = styled(AntdInput)`
`;

export const CloseContainer = styled(ImageContainer)`
  width: 2rem;
`;
