import dynamic from 'next/dynamic';

import _map from 'lodash/map';
import _size from 'lodash/size';
import _indexOf from 'lodash/indexOf';
import _filter from 'lodash/filter';

import MainContent from 'components/MainContent';
import TextEditor from 'components/TextEditor';

import {
  TABS,
  TABS_LABEL_MAP,
  CONTENT,
  EDITOR,
  TERMINAL,
} from './constants';

import {
  Container,
  TabsContainer,
  Tabs,
  BodyContainer,
  ContentContainer,
  EmptyContainer,
  Empty,
} from './body.style';

const Terminal = dynamic(
  () => import('Components/Terminal'),
  { ssr: false }
);

const BODY_COMPOENNTS_ORDER = [
  CONTENT,
  EDITOR,
  TERMINAL,
];

const BODY_COMPOENNTS_MAP = {
  [CONTENT]: MainContent,
  [EDITOR]: TextEditor,
  [TERMINAL]: Terminal,
};

const isSelected = (selectedTabs, tabInfo) => _indexOf(selectedTabs, tabInfo) > -1;

const handleTabChange = (setSelectedTabs, selectedTabs, tabInfo) => () => {
  const isTabSelected = isSelected(selectedTabs, tabInfo);
  const tabs = isTabSelected ? _filter(selectedTabs, tab => tab !== tabInfo) : [...selectedTabs, tabInfo];
  setSelectedTabs(tabs);
};

const renderTabs = (selectedTabs, setSelectedTabs) => (
  <TabsContainer>
    {_map(TABS, tabInfo => (
      <Tabs
        key={tabInfo}
        isSelected={isSelected(selectedTabs, tabInfo)}
        onClick={handleTabChange(setSelectedTabs, selectedTabs, tabInfo)}
      >
        {TABS_LABEL_MAP[tabInfo]}
      </Tabs>
    ))}
  </TabsContainer>
);

const renderBody = (flexBasis, selectedTabs) => (
  <BodyContainer>
    {_map(BODY_COMPOENNTS_ORDER, (tabInfo, index) => {
      const isTabSelected = isSelected(selectedTabs, tabInfo);
      if (!isTabSelected) return null;
      const Component = BODY_COMPOENNTS_MAP[tabInfo];
      return (
        <ContentContainer flexBasis={flexBasis} key={index}>
          <Component />
        </ContentContainer>
      );
    })}
  </BodyContainer>
);

const renderEmptyContent = () => (
  <EmptyContainer>
    <Empty><img src="/logo.png" alt="logo" width="100%" /></Empty>
  </EmptyContainer>
);

const Body = () => {
  const [selectedTabs, setSelectedTabs] = React.useState([CONTENT, EDITOR, TERMINAL]);
  const size = _size(selectedTabs);
  const flexBasis = 100 / size;
  return (
    <Container>
      {renderTabs(selectedTabs, setSelectedTabs)}
      {size ? renderBody(flexBasis, selectedTabs) : renderEmptyContent()}
    </Container>
  );
};

export default Body;
