export const CONTENT = 'CONTENT';
export const EDITOR = 'EDITOR';
export const TERMINAL = 'TERMINAL';

export const TABS = [
  CONTENT,
  EDITOR,
  TERMINAL,
];

export const TABS_LABEL_MAP = {
  [CONTENT]: 'Content',
  [EDITOR]: 'Editor',
  [TERMINAL]: 'Terminal',
};
