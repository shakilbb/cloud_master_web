import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: calc(100% - 1.2rem);
`;

export const TabsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 0.12rem solid #D2CCD6;
`;

export const Tabs = styled.div`
  padding: 0.8rem 1.6rem;
  border: solid #D2CCD6;
  border-width: 0.12rem;
  cursor: pointer;
  border-radius: 0.4rem;
  margin: 0.4rem;
  color: ${props => props.isSelected ? 'white' : 'black'};
  background-color: ${props => props.isSelected ? '#3177D2' : 'white'};
`;

export const BodyContainer = styled.div`
  display: flex;
  height: calc(100% - 4.4rem);
  border: 0.12rem solid #D2CCD6;
`;

export const ContentContainer = styled.div`
  height: 100%;
  display: flex;
  flex-basis: ${props => props.flexBasis}%;
  justify-content: center;
  align-items: center;
  border-right: 0.12rem solid #D2CCD6;
  overflow: auto;
`;

export const EmptyContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  border: 0.12rem solid #D2CCD6;
`;

export const Empty = styled.div`
  width: 28rem;
`;
