// import Body from './Components/Body';
import Header from './Components/PageHeader';
import {
  Container,
  BodyContainer,
} from './homePage.style';

const Home = () => (
  <Container>
    <Header />
    <BodyContainer>
      {/* <Body /> */}
    </BodyContainer>
  </Container>
);

export default Home;
