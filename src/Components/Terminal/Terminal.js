import io from 'socket.io-client';

import TerminalUI from './TerminalUI';
import { Container } from './terminal.style';

const SERVER_ADDRESS = 'http://localhost:8080'; /* SERVER ADDRESS */


const startTerminal = (socket, container) => {
  const terminal = new TerminalUI(socket);
  terminal.attachTo(container);
  terminal.startListening();
};

const connectToSocket = () => new Promise((res) => {
  const socket = io(SERVER_ADDRESS);
  res(socket);
});

const Terminal = () => {
  const containerEl = React.useRef(null);
  const [terminalStatus, setTerminalStatus] = React.useState(false);
  React.useEffect(() => {
    if (terminalStatus) return;
    connectToSocket(SERVER_ADDRESS)
      .then((socket) => {
        startTerminal(socket, containerEl.current);
        setTerminalStatus(true);
      });
  });
  return (
    <Container ref={containerEl} />
  );
};

export default Terminal;
