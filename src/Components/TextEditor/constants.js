export const MODES = [
  'xml',
  'javascript',
];

export const DEFAULT_LANGUAGE = 'javascript';
