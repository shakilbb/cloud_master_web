import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

export const HeaderContainer = styled.div`
  display: flex;
  border-bottom: 0.12rem solid #D2CCD6;
  align-items: center;
  padding: 0.4rem;
  justify-content: flex-end;
`;

export const SelectContainer = styled.div`
  flex-basis: 35%;
`;

export const EditorContainer = styled.div`
  margin-top: 1.2rem;
  height: 100%;
`;

export const SvgContainer = styled.div`
  width: 28px;
  height: 28px;
  cursor: pointer;
  display: flex;
  justify-content: center;
`;
