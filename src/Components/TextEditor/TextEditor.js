import { useState } from 'react';
import { ControlledEditor as Editor } from '@monaco-editor/react';
import Select from 'react-select';

import _once from 'lodash/once';
import _map from 'lodash/map';

import { MODES, DEFAULT_LANGUAGE } from './constants';
import {
  Container,
  HeaderContainer,
  SelectContainer,
  EditorContainer,
} from './textEditor.style';

const getFormattedOptions = _once(() => _map(MODES, key => ({ label: key, value: key })));

const changeLanguage = setLanguage => ({ value }) => setLanguage(value);

const handleInputChange = setValue => (ev, value) => setValue(value);

const TextEditor = () => {
  const [language, setLanguage] = useState(DEFAULT_LANGUAGE);
  const [value, setValue] = useState('// Type Here');
  return (
    <Container>
      <HeaderContainer>
        <SelectContainer>
          <Select
            isSearchable
            onChange={changeLanguage(setLanguage)}
            value={{ label: language, value: language }}
            options={getFormattedOptions()}
            instanceId={language}
            id={language}
          />
        </SelectContainer>
      </HeaderContainer>
      <EditorContainer>
        <Editor
          height="100%"
          language={language}
          options={{ lineNumbers: 'on' }}
          value={value}
          onChange={handleInputChange(setValue)}
        />
      </EditorContainer>
    </Container>
  );
};

export default TextEditor;
