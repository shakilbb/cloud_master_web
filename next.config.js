const withFonts = require('next-fonts');
const withCSS = require('@zeit/next-css');

module.exports = withFonts(withCSS({
  cssLoaderOptions: {
    url: false
  },
  exportPathMap: async function (
    defaultPathMap,
    { dev, dir, outDir, distDir, buildId }
  ) {
    return {
      '/': { page: '/HomePage' },
      '/home': { page: '/HomePage' },
    }
  },
}));
